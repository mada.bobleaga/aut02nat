import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Homework {

    WebDriver driver;

    @DataProvider(name = "logindp")
    public Iterator<Object[]> loginDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // firstame, lastname, email, username, password, confirm password,
        // firstname error message, lastname error message, email error message, username  error message, password error message, conform password error message
        dp.add(new String[]{"", "", "", "", "", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address", "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"test", "", "", "", "", "", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"", "test", "", "", "", "", "Invalid input. Please enter between 2 and 35 letters", "", "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"", "", "madalina.bobleaga@mail.ro", "", "", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters", "",
                "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"", "", "", "test", "", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"", "", "", "", "12345678", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers", "", "Please confirm your password"});
        dp.add(new String[]{"", "", "", "", "", "12345678", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", ""});

        return dp.iterator();

    }

    @Test(dataProvider = "logindp")
    public void negativeLoginTest(String firstName, String lastName, String email, String username, String password, String confirmPassword,
                                  String firstNameErrMsg, String lastNameErrMsg, String emailErrMsg, String usernameErrMsg, String passwordErrMsg,
                                  String confirmPasswordErrMsg) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html#registration_panel");

        WebElement clickYourRegistrationButton = driver.findElement(By.id("register-tab"));
        clickYourRegistrationButton.click();

        WebElement firstNameInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastNameInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement usernameInput = driver.findElement(By.id("inputUsername"));
        WebElement passwordInput = driver.findElement(By.id("inputPassword"));
        WebElement confirmPasswordInput = driver.findElement(By.id("inputPassword2"));
        WebElement submitYourRegistrationButton = driver.findElement(By.id("register-submit"));

        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        confirmPasswordInput.clear();
        confirmPasswordInput.sendKeys(confirmPassword);
        submitYourRegistrationButton.submit();

        WebElement errmsg1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
        WebElement errmsg2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
        WebElement errmsg3 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]"));
        WebElement errmsg4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
        WebElement errmsg5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
        WebElement errmsg6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));

    /*    System.out.println(errmsg1.getText());
        System.out.println(errmsg2.getText());
        System.out.println(errmsg3.getText());
        System.out.println(errmsg4.getText());
        System.out.println(errmsg5.getText());
        System.out.println(errmsg6.getText());
        */

        Assert.assertEquals(errmsg1.getText(), firstNameErrMsg);
        Assert.assertEquals(errmsg2.getText(), lastNameErrMsg);
        Assert.assertEquals(errmsg3.getText(), emailErrMsg);
        Assert.assertEquals(errmsg4.getText(), usernameErrMsg);
        Assert.assertEquals(errmsg5.getText(), passwordErrMsg);
        Assert.assertEquals(errmsg6.getText(), confirmPasswordErrMsg);


        driver.close();


    }
}
