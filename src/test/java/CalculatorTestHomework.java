import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.curs7.Calculator;

public class CalculatorTestHomework {

    static Calculator c;

    @BeforeClass
    public static void beforeTest() {
        c = new Calculator();
    }

    @Test
    public void test1Homework() {
        Assert.assertEquals(25, c.compute(17, 8, "+"), 0);
    }

    @Test
    public void test2Homework() {
        Assert.assertEquals(258000, c.compute(9632, 248368, "+"), 0);
    }

    @Test
    public void test3Homework() {
        Assert.assertEquals(5, c.compute(2.5, 2.5, "+"), 0);
    }

    @Test
    public void test4Homework() {
        Assert.assertEquals(-174, c.compute(789, 963, "-"), 0);
    }

    @Test
    public void test5Homework() {
        Assert.assertEquals(857462899, c.compute(862358586, 4895687, "-"), 0);
    }

    @Test
    public void test6Homework() {
        Assert.assertEquals(868370151, c.compute(26589, 32659, "*"), 0);
    }

    @Test
    public void test7Homework() {
        Assert.assertEquals(228.69864, c.compute(32.859, 6.96, "*"), 0);

    }

    @Test
    public void test8Homework() {
        Assert.assertEquals(0, c.compute(0, 6.96, "*"), 0);

    }

    @Test
    public void test9Homework() {
        Assert.assertEquals(2.4, c.compute(12, 5, "/"), 0);

    }

    @Test
    public void test10Homework() {
        Assert.assertEquals(4.0153, c.compute(261, 65, "/"), 0.0001);

    }

    @Test
    public void test11Homework() {
        Assert.assertEquals(8, c.compute(64, 8, "/"), 0);

    }

    @Test(expected = IllegalArgumentException.class)
    public void test12Homework() {
        Assert.assertEquals(63, c.compute(6, 88, "."), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test13Homework() {
        Assert.assertEquals(63, c.compute(6, 88, "f"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test14Homework() {
        Assert.assertEquals(63, c.compute(6, 88, "&"), 0);
    }

    @Test
    public void test15Homework() {
        Assert.assertEquals(2, c.compute(4, 0, "SQRT"), 0);
    }

    @Test
    public void test16Homework() {
        Assert.assertEquals(2.6457, c.compute(7, 0, "SQRT"), 0.0001);
    }
}



