package ro.siit.curs3;

public class Numarator {

    public static void main(String[] args) {

        final int MAX_NUM = 25;
        int count = 1;
        while (count < 25) {
            System.out.println(count);
            count++;
            System.out.println(" Noua valoare este " + count);
        }

        count = 1;
        do {
            System.out.println(count);
            count++;
        }
        while (count < MAX_NUM);

        for (int c = 1 ; c < MAX_NUM; c++) {
            System.out.println(c);
        }

        // Iterare prin lista
        int [] lista = {1,5,7,78,12};

        for (int i = 0; i< lista.length; i++) {
            System.out.println(lista[i]);
        }

    }
}
