package ro.siit.curs3.JavaHomework;

public class LeapYear {

    public static void main(String[] args) {


        //1. introducem anul
        int year = Integer.parseInt(args[0]);

        //2. Boundary analysis - Facem o restrictie sa fie anul intre 1900-2016
        //3. identificam daca anul este leap si afisam cate zile are Februarie in anul respectiv
        //4. afisam daca anul este sau nu leap
        if (year < 1900 || year > 2016) {
            System.out.println("The number is not in the range of 1900-2016");
        } else {
            if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) { //anul este leap - February are 29 zile
                System.out.println("February has 29 days in the year " + year);
            }

            if (year % 4 == 0 && year % 100 != 0) {
                System.out.println("February has 29 days in the year " + year);
            }

            if (year % 4 != 0) { //anul nu este leap - February are 28 zile
                System.out.println("February has 28 days because the year " + year + " is not a leap year");
            }

            if (year % 4 == 0 && year % 100 == 0 && year % 400 != 0) {
                System.out.println("February has 28 days because the year " + year + " is not a leap year");
            }
        }
    }
}
