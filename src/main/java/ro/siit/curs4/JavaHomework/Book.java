package ro.siit.curs4.JavaHomework;

public class Book {

    public String name;
    public int year;
    public Author author;
    public Double price;

    public Book(String name, int year, Author author, Double price) {
    this.name = name;
    this.year = year;
    this.author = author;
    this.price = price;

    }

    public Book() {

    }

    public String getName() {
        return this.name;
    }

    public int getYear() {
        return this.year;
    }
    public Author getAuthor() {
        return this.author;
    }
    public Double getPrice() {
        return this.price;
    }


    }
