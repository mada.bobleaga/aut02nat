package ro.siit.curs4.JavaHomework;

public class Author {

    public String name;
    public String email;

    public Author (String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Author() {

    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

}
