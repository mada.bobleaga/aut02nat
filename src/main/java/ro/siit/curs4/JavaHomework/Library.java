package ro.siit.curs4.JavaHomework;

public class Library {

    public static void main(String[] args) {

        Author a1 = new Author();
        a1.name = "Madalina Bobleaga";

        Book b1 = new Book();
        b1.name = "Automation testing";
        b1.price = 200.96;
        b1.year = 2010;

        System.out.println("Numele cartii este " +b1.name + " (pret " + b1.price + " RON),"  + " publicata in anul " + b1.year + ".");
    }
}

