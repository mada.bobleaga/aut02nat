package ro.siit.curs4;

public class Draw {

    public static void main(String[] args) {
        drawFullShape(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutline(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();

        drawFullShape(Integer.parseInt(args[0]));
        System.out.println();
        drawShapeOutline(Integer.parseInt(args[0]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]));
    }

    private static void drawShapeCorners(int weight, int height) {
      for (int j = 0; j < height; j++) {
          for (int i = 0; i < weight; i++) {
              if (j == 0 || j == height - 1) {  // daca suntem pe prima sau pe ultima linie afisam numai stelute
                      if (i==0 || i == weight - 1) { // daca suntem pe primul sau ultimul caracter de pe linie
                          System.out.print("*");
                      }
                      else { // daca nu suntem pe primul sau ultimul caracter de pe linie
                          System.out.print(" ");
                     }
                  }
                  else { // daca nu suntem pe prima sau ultima linie
                  System.out.print(" ");
                 }
              }
          System.out.println();
      }
    }

    private static void drawShapeCorners(int l) {
        drawShapeCorners(l, l);
    }

    private static void drawShapeOutline(int weight, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < weight; i++) {
                if (j==0 || j==height-1) { // daca suntem pe prima sau pe ultima linie afisam numai stelute
                    System.out.print("*");
                }
                else { // daca nu suntem pe prima sau ultima linie
                    if (i==0 || i == weight - 1) { // daca suntem pe primul sau ultimul caracter de pe linie
                        System.out.print("*");
                    }
                    else { // daca nu suntem pe primul sau ultimul caracter de pe linie
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    private static void drawShapeOutline(int l) {
        drawShapeOutline(l, l);
    }

    public static void drawFullShape(int weight, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < weight; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void drawFullShape(int l) {
    drawFullShape(l, l);
    }
}
