package ro.siit.curs4;

public class Square {

    private double squareSide;

    public Square() {

    }
    public Square(double squareSide) {
        this.squareSide = squareSide;
    }

    protected void setSide(double squareSide) {
        this.squareSide = squareSide;
    }

    public double getArea() {
        //return squareSide * squareSide;
        return Math.pow(squareSide, 2); //squareside la patrat
    }

    void printSquare() {
        System.out.println("Avem un patrat cu latura " + squareSide + " si cu aria " + getArea());
    }



}
