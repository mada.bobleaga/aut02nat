package ro.siit.curs6.JavaHomework;

import java.util.*;

public class Building {

    public static void displayList(List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void showList() {
        List arrayList = new ArrayList();
        arrayList.add("First floor");
        arrayList.add("Second floor");
        arrayList.add("Third floor");

        displayList(arrayList);
    }

    public static void main(String[] args) {
        showList();
    }

}


