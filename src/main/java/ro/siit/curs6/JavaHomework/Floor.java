package ro.siit.curs6.JavaHomework;

import java.util.HashMap;
import java.util.Map;

public class Floor {

    public static void showHashMap1() {
        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Office space ", 1);
        hashMap.put("Toilets", 2);
        hashMap.put("Kitchen", 1);
        hashMap.put("Conference rooms", 3);
        System.out.println("First floor has " + hashMap);

    }
        public static void showHashMap2() {
            Map<String, Integer> hashMap = new HashMap<>();
            hashMap.put("Office spaces ", 2);
            hashMap.put("Toilets ", 2);
            hashMap.put("Kitchen ", 1);
            hashMap.put("Conference rooms ", 4);
            System.out.println("Second floor has " + hashMap);

        }
        public static void showHashMap3() {
            Map<String, Integer> hashMap = new HashMap<>();
            hashMap.put("Toilets ", 2);
            hashMap.put("Conference rooms ", 6);
            System.out.println("Third floor has " + hashMap);

        }

        public static void main(String[] args) {

            showHashMap1();
            showHashMap2();
            showHashMap3();

        }
}



