package ro.siit.curs6.JavaHomework;

public enum Room {
    OFFICE_SPACES,
    TOILETS,
    KITCHEN,
    CONFERENCE_ROOMS
}
