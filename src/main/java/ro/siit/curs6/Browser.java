package ro.siit.curs6;

public enum Browser {
    CHROME,
    IE,
    FIREFOX,
    EDGE,
    SAFARI
}

