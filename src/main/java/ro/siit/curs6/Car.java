package ro.siit.curs6;

public interface Car {
//metode
    public void startEngine();

    public void stopEngine();

    public void accelerate(Integer Delta);

    public void brake();

    public void shiftUp();
}
