package ro.siit.curs6;

import java.util.*;
import java.util.List;

public class CollectionsEx {

    public static void showArrays() {
        int[] arrays = {1, 3, 4, 5};
        System.out.println("Element poz 3:" + arrays[2]);

        Car[] Cars = {new Truck(), new Truck()};

        //display entiere array
        for (int i = 0; i <= arrays.length - 1; i++) {
            System.out.println(arrays[i]);
        }

        //display with forEach
        for (int el : arrays) {
            System.out.println(el);
        }

        String[] names = {"Ion", "Maria", "Vasile"};
        for (String name : names) {
            System.out.println(name);
        }
    }

    public static void displayList(List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void showList() {
        List arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add("Ana");
        arrayList.add("Ana are mere");
        arrayList.add(1, 2);
        displayList(arrayList);

        arrayList.remove("Ana");
        displayList(arrayList);

        List<String> names = new ArrayList<String>();
        names.add("Ion");
        names.add("Vasile");
        names.add("Vasile");
        names.add("Ana");
        System.out.println("Size of names:" + names.size() + "->" + names.toString());
        System.out.println(names.get(1));

//      sort the list
        Collections.sort(names);
        System.out.println(names);
    }

    //set - list with unique elements
    public static void showSets() {
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);
        setList.add(3);

        System.out.println(setList.toString());
    }

    public static void displayMap(Map hashMap) {
        List keys = new ArrayList<>(hashMap.keySet());
        Collections.sort(keys);
        for (Object key : keys) {
//          key -> values
            System.out.println(key + "->" + hashMap.get(key));
        }
    }

    public static void showHashMap() {
        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("laptop", 3);
        hashMap.put("pc", 5);
        hashMap.put("pc", 2);
        hashMap.put("mobile", 4);

        displayMap(hashMap);
    }


    public static void main(String[] args) {
  //     showArrays();
   //     showList();
//        showSets();
        showHashMap();
    }
}